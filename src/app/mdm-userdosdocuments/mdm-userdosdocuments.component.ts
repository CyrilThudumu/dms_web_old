import { MatTableDataSource, MatDialogConfig, MatDialog } from '@angular/material';
import { Component, OnInit } from '@angular/core';
import { PatientService } from '../patient.service';
import { Router, ActivatedRoute } from '@angular/router';
import { DataService } from '../_services/data.service';
import { attorneydos } from '../_models/attorneydos';
import { DisplayPatientDocsComponent } from '../display-patient-docs/display-patient-docs.component';
import { AssigndostoattorneyComponent } from '../assigndostoattorney/assigndostoattorney.component';

@Component({
  selector: 'app-mdm-userdosdocuments',
  templateUrl: './mdm-userdosdocuments.component.html',
  styleUrls: ['./mdm-userdosdocuments.component.css']
})

  export class MdmUserdosdocumentsComponent  implements OnInit {

  dataSource;
  isDisabled = true;
  dateOfservice$;
  id;
  patientr;
  currentPatient=[];
  displayedColumns = ['patientId','Name', 'Age', 'gender', 'phone', 'Practice', 'doa', 'doctor'];
 
  doss: any;
  indexx: any;
  document: any;
  nwdata: any;
  attorneyDos:attorneydos[] =JSON.parse(localStorage.getItem('attorneydos')) || [];
 status:string;
 statusDate:Date;
 paymentDate:Date;
 arbNotes:string;
 natureOfDispute:string;
  attorney: any;
  constructor( 
    private patientService : PatientService,
     private router: Router, 
      private route: ActivatedRoute,
       private dataService:DataService,
    public dialog:MatDialog,
  ) { 
    //super();
  
    this.id = this.route.snapshot.paramMap.get('id');
    console.log('patient id is ', this.id)
    if (this.id) {
     this.patientr = this.patientService.getattorneydos(this.id);
      this.currentPatient[0] =this.patientService.get(this.id);
      //.subscribe(o => this.patient = o);
      console.log('currentpatient and patient are ', this.patientr)
     
   }   
  }
  ngOnInit() {
    this.dataSource = new MatTableDataSource(this.currentPatient);
   console.log(this.dataSource);
  }


  save(){
    console.log('save ', this.nwdata);
let newatorneydos=this.nwdata;
this.attorneyDos.push(newatorneydos);
    localStorage.setItem('attorneydos', JSON.stringify(this.attorneyDos));
    this.dataService.setData(this.nwdata);
   // this.router.navigate(['/mdmuser/patients']);
   }
  openAssignAttorneyDialog(dos, index, patient, event:any){
    event.preventDefault();
    const dialogConfig = new MatDialogConfig();
  
    console.log('patientr is ',this.patientr)
   this.doss=dos;
   this.indexx=index;
  
   dialogConfig.panelClass="assignAttorney";
   dialogConfig.id="practiceForm";
   dialogConfig.data={dos:this.doss, index:this.indexx, doc:patient,  pdfSrc:this.document};
   const dialogRef = this.dialog.open(AssigndostoattorneyComponent, dialogConfig);
    dialogRef.componentInstance.onAdd.subscribe((data) => {
    this.nwdata=data;
  });
 
   }
  }

