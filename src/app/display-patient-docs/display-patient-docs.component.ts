import { Component, OnInit } from '@angular/core';
import { MatTableDataSource, MatDialogConfig, MatDialog } from '@angular/material';
import { PatientService } from '../patient.service';
import { Router, ActivatedRoute } from '@angular/router';
import { doc } from '../_models/doc';
import { AssigndostoattorneyComponent } from '../assigndostoattorney/assigndostoattorney.component';
import { DataService } from '../_services/data.service';
import { attorneydos } from '../_models/attorneydos';
@Component({
  selector: 'app-display-patient-docs',
  templateUrl: './display-patient-docs.component.html',
  styleUrls: ['./display-patient-docs.component.css']
})
export class DisplayPatientDocsComponent implements OnInit {
  id: string;
  patient: doc[]=[];
  doss: any;
  indexx: any;
  document: any;
  nwdata: any;
  docData:any;
   attorneyDos:attorneydos[] =JSON.parse(localStorage.getItem('attorneydos')) || [];
   //storagemodel :doc[] =JSON.parse(localStorage.getItem('model')) || [];
   
  constructor(
    private patientService : PatientService,
    private router: Router, 
    private route: ActivatedRoute,
    public dialog:MatDialog ,
    private dataService:DataService,
    
  ) { 
    this.id = this.route.snapshot.paramMap.get('id');
    console.log('id is ',this.id);
    this.docData = localStorage.getItem('model')
    
 
    if (this.id) {
      this.patient = this.patientService.getPatientById(this.id); 
        }
  
  
  }
  async delay(ms: number) {
    await new Promise(resolve => setTimeout(()=>resolve(), ms)).then(()=>console.log("wait for the option to select"));
  }
  ngOnInit() {
   // console.log('in display', this.document);
 //  this.patientService.openAssignAttorneyDialog(dos: any, index: any, patient: any, event: any);
  }
  
  Update(){
    console.log('update ', this.nwdata);
    
let newatorneydos=this.nwdata;
this.attorneyDos.push(newatorneydos);
    localStorage.setItem('attorneydos', JSON.stringify(this.attorneyDos));
    this.dataService.setData(this.nwdata);
    this.router.navigate(['/mdmuser/patients']);
   }
  openAssignAttorneyDialog(dos, index, patient, event:any){
    event.preventDefault();
    const dialogConfig = new MatDialogConfig();
   this.doss=dos;
   this.indexx=index;
   dialogConfig.panelClass="assignAttorney";
   dialogConfig.id="practiceForm";
   dialogConfig.data={dos:this.doss, index:this.indexx, doc:patient,  pdfSrc:this.document};
   const dialogRef = this.dialog.open(AssigndostoattorneyComponent, dialogConfig);
    dialogRef.componentInstance.onAdd.subscribe((data) => {
    this.nwdata=data;
  });
 
   }

   
}
